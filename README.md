# README
## Development env
You can setup app using native approach or use Docker

## Native approach:
### Install RVM
    \curl -sSL https://get.rvm.io | bash -s stable --ruby
    
### Install System dependencies and libraries
   1. `Install postgres`: [Postgres install](https://www.postgresql.org/download/)
   2. `Install redis`: [Redis quick start](https://redis.io/topics/quickstart)
   3. `Install nodejs`: [Install nodejs](https://nodejs.org/en/download/)
   4. `Install yarn`: [Install yarn](https://classic.yarnpkg.com/en/docs/install/)

### Ruby version
    rvm install 2.7.0
    rvm use 2.7.0@marley --create --ruby-version
    
### Configuration
    create .env file in the root of the app(TIP: take a look at .env.example file)
    gem install bundler -v 2.1.4
    gem install foreman
    bundle install
    yarn install

### Database creation
    rake db:create

### Database initialization
    rake db:schema:load
    
### Start the app
    foreman start -f Procfile.dev    

### How to run the test suite
    rspec ./spec

# Running with Docker

##Docker approach
### Start a local Docker Development Environment:

 1. [Install Doker](https://docs.docker.com/install/)
 2. [Install Docker compose](https://docs.docker.com/compose/install/)
 2. `git clone git@gitlab.com:baramik1989/marley-test.git`
 3. `cd marley-test`
 4. `docker-compose up --build`
 5. `docker-compose run web bundle exec rails db:create`
 6. `docker-compose run web bundle exec rails db:schema:load`