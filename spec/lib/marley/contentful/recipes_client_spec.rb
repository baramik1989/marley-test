require 'rails_helper'

describe Marley::Contentful::RecipesClient do
  describe '.fetch_recipes' do
    it 'returns Marley::Contentful::ApiResponse::Entries::Recipes object' do
      VCR.use_cassette('fetch_recipes') do
        result = described_class.fetch_recipes
        expect(result).to be_kind_of(Marley::Contentful::ApiResponse::Entries::Recipes)
        expect(result).to respond_to(:total)
        expect(result).to respond_to(:skip)
        expect(result).to respond_to(:limit)
        expect(result).to respond_to(:items)
      end
    end

    it 'responds to proper attributes' do
      VCR.use_cassette('fetch_recipes') do
        result = described_class.fetch_recipes
        expect(result).to respond_to(:total)
        expect(result).to respond_to(:skip)
        expect(result).to respond_to(:limit)
        expect(result).to respond_to(:items)
        expect(result.items.first).to be_kind_of(Marley::Contentful::ApiResponse::Entries::Recipe)
      end
    end
  end
  describe '.fetch_recipe' do
    context 'when incorrect id passed' do
      it 'returns Marley::Contentful::ApiResponse::Entries::Recipe object' do
        VCR.use_cassette('fetch_recipe') do
          result = described_class.fetch_recipe(recipe_id: "437eO3ORCME46i02SeCW46")
          expect(result).to be_kind_of(Marley::Contentful::ApiResponse::Entries::Recipe)
        end
      end
      it 'responds to proper attributes' do
        VCR.use_cassette('fetch_recipe') do
          result = described_class.fetch_recipe(recipe_id: "437eO3ORCME46i02SeCW46")
          expect(result).to respond_to(:title)
          expect(result).to respond_to(:description)
          expect(result).to respond_to(:image_url)
          expect(result).to respond_to(:tags)
          expect(result).to respond_to(:chef_name)
          expect(result).to respond_to(:external_id)
        end
      end
    end

    context 'when correct id passed' do
      it 'returns nil' do
        VCR.use_cassette('fetch_recipe_dummy') do
          result = described_class.fetch_recipe(recipe_id: "dummy")
          expect(result).to eq(nil)
        end
      end
    end
  end
end