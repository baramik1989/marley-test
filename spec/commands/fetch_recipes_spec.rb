require 'rails_helper'

describe FetchRecipes do
  describe '.call' do
    context 'exception caught' do
      before do
        allow(SyncRecipesJob).to receive(:perform_later).and_raise(StandardError)
      end
      it 'broadcasts :error with message' do
        VCR.use_cassette('fetch_recipes') do
          expect(described_class.call).to have_key(:error)
        end
      end
    end

    context 'recipes fetched' do
      it 'broadcasts :ok with recipes' do
        VCR.use_cassette('fetch_recipes') do
          result = described_class.call
          expect(result).to have_key(:ok)
          expect(result[:ok]).to be_kind_of(Marley::Contentful::ApiResponse::Entries::Recipes)
        end
      end
    end
  end
end