require 'rails_helper'

describe FetchRecipe do
  describe '.call' do
    context 'exception caught' do
      before do
        allow(Marley::Contentful::RecipesClient).to receive(:fetch_recipe).and_raise(StandardError)
      end
      it 'broadcasts :error with message' do
        VCR.use_cassette('fetch_recipe') do
          result = described_class.call(recipe_id: '437eO3ORCME46i02SeCW46')
          expect(result).to have_key(:error)
        end
      end
    end

    context 'recipe fetched' do
      context 'db record is absent' do
        it 'broadcasts :ok with api recipe object' do
          VCR.use_cassette('fetch_recipe') do
            result = described_class.call(recipe_id: '437eO3ORCME46i02SeCW46')
            expect(result).to have_key(:ok)
            expect(result[:ok]).to be_kind_of(Marley::Contentful::ApiResponse::Entries::Recipe)
          end
        end
      end
      context 'db record is present' do
        it 'broadcasts :ok with api recipe object' do
          create(:recipe)
          result = described_class.call(recipe_id: '437eO3ORCME46i02SeCW46')
          expect(result).to have_key(:ok)
          expect(result[:ok]).to be_kind_of(String)
        end
      end
    end

    context 'records not found' do
      before do
        allow_any_instance_of(FetchRecipe).to receive(:recipe).and_return(nil)
      end
      it 'broadcasts :not_found' do
        result = described_class.call(recipe_id: 'dumb')
        expect(result).to have_key(:not_found)
      end
    end
  end
end