FactoryBot.define do
  factory :recipe do
    title { "Crispy Chicken and Rice\twith Peas & Arugula Salad" }
    description {"Crispy chicken skin, tender meat, and rich, tomato..."  }
    image_url { "https://images.ctfassets.net/kk2bw5ojx476/5mFyTozv..." }
    tags { ["gluten free", "healthy"] }
    chef_name { "Jony Chives" }
    external_id { "437eO3ORCME46i02SeCW46" }
  end
end
