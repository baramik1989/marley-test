Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'home#index'

  namespace :api, defaults: {format: :json} do
    namespace :v1 do
      resources :recipes, only: [:index] do
        collection do
          get ':external_id', to: 'recipes#show'
        end
      end
    end
  end

  match '*path', to: 'home#index', via: :all
end
