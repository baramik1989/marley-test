import React, {useState, useEffect} from 'react'
import { Link } from "react-router-dom";
import '../styles/recipes.css'

function Recipes() {
    const [recipes, setRecipes] = useState([])

    useEffect(() => {
        let cleanupFunction = false;
        const url = "/api/v1/recipes";
        fetch(url)
            .then(response => {
                if (response.ok) {
                    return response.json();
                }
            })
            .then(response => {
                if(!cleanupFunction) setRecipes(response.items)
            })

        return () => cleanupFunction = true;
    }, [])

    const recipeItems = recipes.map((recipe, index) => {
        return(
            <div className="card recipe-item" key={index}>
                <img src={recipe.image_url+"?w=400"} className="card-img-top" alt="Recipe Image"/>
                    <div className="card-body">
                        <Link to={`/recipes/${recipe.external_id}`} className="card-title">{recipe.title}</Link>
                    </div>
            </div>
        )
        }
    );

    return(
        <div className="container-fluid">
            <h1>Recipes list</h1>
            <div className="row">{recipeItems}</div>
        </div>
    )
}

export default Recipes