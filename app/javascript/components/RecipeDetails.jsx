import React, {useState, useEffect, Fragment} from 'react'
import ReactMarkdown from 'react-markdown'
import { useParams } from 'react-router-dom'

function RecipeDetails() {
    const [recipe, setRecipe] = useState({})
    let params  = useParams()

    useEffect(() => {
        let cleanupFunction = false;
        const url = `/api/v1/recipes/${params.external_id}`;
        fetch(url)
            .then(response => {
                if (response.ok) {
                    return response.json();
                }
            })
            .then(response => {
                if(!cleanupFunction) setRecipe(response)
            })
        return () => cleanupFunction = true;

    }, [])

    let tags;
    if (recipe.tags) {
       tags = recipe.tags.map((tag, index) => {
                <span key={index}>{tag}</span>
            }
        )
    } else {
        tags = <span>None</span>
    }

    return(
        <div className="container-fluid">
            <div className="row">
                <div className="col-sm">
                    <img src={recipe.image_url}/>
                </div>
                <div className="col-sm">
                    <h1>{recipe.title}</h1>
                    <p>Tags: {tags}</p>
                    <p>Chef: {recipe.chef_name === '' ? 'none': recipe.chef_name}</p>
                    <span>Description: </span><ReactMarkdown>{recipe.description}</ReactMarkdown>
                </div>
            </div>
        </div>
    )

}

export default RecipeDetails
