import React from 'react';
import RecipeDetails from "./RecipeDetails";
import Recipes from "./Recipes";
import {Switch, Route, BrowserRouter as Router} from 'react-router-dom';

export default class Main extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <Router>
                <Switch>
                    <Route path="/" exact component={Recipes} />
                    <Route path="/recipes" exact component={Recipes} />
                    <Route path="/recipes/:external_id" exact component={RecipeDetails} />
                </Switch>
            </Router>
        )
    }
}