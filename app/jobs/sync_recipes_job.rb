class SyncRecipesJob < ApplicationJob
  def perform(recipes:)
    recipes.map! do |recipe|
      recipe[:created_at] = Time.now
      recipe[:updated_at] = Time.now
      recipe
    end
    Recipe.upsert_all(recipes, unique_by: :external_id)
  end
end