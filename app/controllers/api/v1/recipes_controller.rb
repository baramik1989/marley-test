module Api
  module V1
    class RecipesController < ApiController
      def index
        result = FetchRecipes.call

        return error(result[:error]) if result[:error]

        success(result[:ok])
      end

      def show
        result = FetchRecipe.call(recipe_id: params[:external_id])

        return error(result[:error]) if result[:error]
        return success(result[:not_found]) if result[:not_found]

        success(result[:ok])
      end
    end
  end
end