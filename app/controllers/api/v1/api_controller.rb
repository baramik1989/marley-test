module Api
  module V1
    class ApiController < ApplicationController

      def success(payload)
        render json: payload, status: :ok
      end

      def error(payload)
        render json: payload, status: :unprocessable_entity
      end
    end
  end
end