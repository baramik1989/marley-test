class FetchRecipe < BaseService
  def initialize(recipe_id:)
    @recipe_id = recipe_id
  end

  def call
    return broadcast(:not_found, {message: 'Recipe not found'}) unless recipe

    broadcast(:ok, recipe)
  rescue StandardError => e
    broadcast(:error, {message: 'Something went wrong', exception: e})
  end

  private

  attr_reader :recipe_id

  def recipe
    @recipe ||= recipe_db_as_json || api_recipe
  end

  def recipe_db_as_json
    db_recipe.to_json(except: [:created_at, :updated_at, :id]) if db_recipe
  end

  def db_recipe
    #TODO: It's better to use any kind of serialization for api response and db_recipe
    @recipe ||= Recipe.find_by(external_id: recipe_id)
  end

  def api_recipe
    Marley::Contentful::RecipesClient.fetch_recipe(recipe_id: recipe_id)
  end
end