class FetchRecipes < BaseService
  def call
    response = Marley::Contentful::RecipesClient.fetch_recipes
    #bulk upsert recipes to save api calls
    SyncRecipesJob.perform_later(recipes: response.items)

    broadcast(:ok, response)
  rescue StandardError
    broadcast(:error, {message: 'Something went wrong'})
  end
end