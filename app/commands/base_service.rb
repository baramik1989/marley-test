class EventRecorder
  attr_accessor :events

  def initialize(events = {})
    @events = events
  end

  def save_event(event)
    self.events = event
  end
end

class BaseService
  attr_accessor :event_recorder

  def self.call(*args, &block)
    instance = new(*args)
    instance.event_recorder = EventRecorder.new
    instance.call
    instance.event_recorder.events
  end

  def call
    raise NotImplementedError
  end

  def transaction(&block)
    ActiveRecord::Base.transaction(&block) if block_given?
  end

  def broadcast(event, output=[])
    return {} unless event

    event_recorder.save_event({event => output})
  end
end
