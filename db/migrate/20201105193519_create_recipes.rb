class CreateRecipes < ActiveRecord::Migration[6.0]
  def change
    create_table :recipes do |t|
      t.string :title, index: true
      t.text :description
      t.string :image_url
      t.jsonb :tags, index: true
      t.string :chef_name, index: true
      t.string :external_id

      t.timestamps
    end
  end
end
