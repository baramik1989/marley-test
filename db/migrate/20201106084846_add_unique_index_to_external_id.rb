class AddUniqueIndexToExternalId < ActiveRecord::Migration[6.0]
  def change
    add_index(:recipes, :external_id, unique: true)
  end
end
