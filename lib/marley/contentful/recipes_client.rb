module Marley
  module Contentful
    class RecipesClient
      class << self
        def fetch_recipes
          response = client.entries(content_type: 'recipe')

          return unless response

          Marley::Contentful::ApiResponse::Entries::Recipes.new(context: response)
        end

        def fetch_recipe(recipe_id:)
          response = client.entry(recipe_id)

          return unless response

          Marley::Contentful::ApiResponse::Entries::Recipe.new(context: response)
        end

        private

        def client
          ::Contentful::Client.new(space:           ENV['CONTENTFUL_SPACE'],
                                   access_token:    ENV['CONTENTFUL_ACCESS_TOKEN'],
                                   environment:     ENV['CONTENTFUL_ENV'],
                                   dynamic_entries: :auto,
                                   logger:          Rails.logger)
        end
      end
    end
  end
end