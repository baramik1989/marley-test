module Marley
  module Contentful
    module ApiResponse
      class BaseResponse < ::Hashie::Trash
        include ::Hashie::Extensions::Dash::Coercion
        include ::Hashie::Extensions::IndifferentAccess
        include ::Hashie::Extensions::Dash::PropertyTranslation
      end
    end
  end
end