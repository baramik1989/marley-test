module Marley
  module Contentful
    module ApiResponse
      module Entries
        class Recipes < BaseResponse
          property :total, from: :context, with: ->(value) { value.total }
          property :skip, from: :context, with: ->(value) { value.skip }
          property :limit, from: :context, with: ->(value) { value.limit }
          property :items, from: :context, with: ->(value) do
            value.map do |recipe|
              Marley::Contentful::ApiResponse::Entries::Recipe.new(context: recipe)
            end
          end
        end
      end
    end
  end
end