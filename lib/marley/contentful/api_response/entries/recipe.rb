module Marley
  module Contentful
    module ApiResponse
      module Entries
        class Recipe < BaseResponse
          get_chef_name = ->(context) do
            context.chef.name
          rescue ::Contentful::EmptyFieldError
            ''
          end

          get_tags = ->(context) do
            context.tags.map(&:name)
          rescue ::Contentful::EmptyFieldError
            ''
          end

          property :title, from: :context, with: ->(value) { value.title.gsub('\t', ' ') }
          property :description, from: :context, with: ->(value) { value.description }
          property :image_url, from: :context, with: ->(value) { "https:" + value.photo.url }
          property :tags, from: :context, with: get_tags
          property :chef_name, from: :context, with: get_chef_name
          property :external_id, from: :context, with: ->(value) { value.id }
        end
      end
    end
  end
end